//
//  String+Extensions.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 28/4/21.
//

import UIKit

//Extension that allow us to get a predifined color from a string
extension String {
    func getColorFromString() -> UIColor {
        switch self {
        case "colorCyan": return UIColor.blue
        case "colorRed": return UIColor.red
        case "colorPurple": return UIColor.purple
        default:
            return UIColor.white
        }
    }
}
