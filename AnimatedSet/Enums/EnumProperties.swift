//
//  EnumProperties.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 28/4/21.
//

import Foundation

//enum that has the differnt properties that we will use with our cards
enum EnumProperties {
    enum AmountOfCards: Int {
        case amountOne = 1
        case amountTwo = 2
        case amountThree = 3
    }
    
    enum TypeOfShape: String {
        case typeDiamond
        case typeOval
        case typeSquiggle
    }
    
    enum TypeOfShading: Int {
        case typeFill = 1
        case typeBorder = 2
        case typeStripes = 3
    }
    
    enum TypeOfColor: String {
        case colorCyan
        case colorRed
        case colorPurple
    }
}
