//
//  EnumGameStatus.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 28/4/21.
//

import Foundation

//Enum that has all the game status of our Set Game
enum EnumGameStatus: Int {
  case startGame
  case gameIdle
  case addThreeCards
  case removeSetFlying
  case removeSetToDeck
  case shuffleCardsFlyingToDeck
  case shuffleCardsExplotion
  case shuffleCardsToPosition
  case replaceSet
  case replaceSetEndGame
  case pickCard
  case idle
}
