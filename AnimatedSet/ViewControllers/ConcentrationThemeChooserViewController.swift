//
//  ConcentrationThemeChooserViewController.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 2/5/21.
//

import UIKit

class ConcentrationThemeChooserViewController: UIViewController, UISplitViewControllerDelegate {

  private var splitViewDetailConcentrationViewController: ConcentrationViewController? {
    return splitViewController?.viewControllers.last as? ConcentrationViewController
  }
  private var lastSeguedToConcentrationViewController: ConcentrationViewController?
  
  override func awakeFromNib() {
    splitViewController?.delegate = self
  }
  
  func splitViewController(
    _ splitViewController: UISplitViewController,
    collapseSecondary secondaryViewController: UIViewController,
    onto primaryViewController: UIViewController
  ) -> Bool {
      if let cvc = secondaryViewController as? ConcentrationViewController {
        return cvc.theme == nil
      }
      return false
    }
  
  //Array of themes, to add another theme just create a new Theme object and pick its cardColor, backgroundColor and 8 emojis
  var themes = [
    "Halloween" : Theme(cardColor: #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), backgroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), emojis: ["👹","👻","💀","🎃","🦇","🕷","🔮","😈"]),
      "Christmas" : Theme(cardColor: #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), backgroundColor: #colorLiteral(red: 0.6574712396, green: 0.924770236, blue: 0.9070860744, alpha: 1), emojis: ["☃️","❄️","🧤","🧣","🎁","🎄","🎅🏼","🤶🏼"]),
    "Animals" : Theme(cardColor: #colorLiteral(red: 0.3019564748, green: 0.08780188113, blue: 0.5115540624, alpha: 1), backgroundColor: #colorLiteral(red: 0.02227502316, green: 0.9151110968, blue: 0.6023659562, alpha: 1), emojis: ["🐱","🐶","🐰","🦁","🐷","🐸","🐻","🐵"]),
    "Plants" : Theme(cardColor: #colorLiteral(red: 0.1404665541, green: 0.3164428557, blue: 0, alpha: 1), backgroundColor: #colorLiteral(red: 0.5591451016, green: 0.7910486857, blue: 0.5920773633, alpha: 1), emojis: ["🌲","🌴","🌷","🌺","💐","🌻","🍀","🌼"]),
    "Faces" : Theme(cardColor: #colorLiteral(red: 0.4026800599, green: 0.395570013, blue: 0, alpha: 1), backgroundColor: #colorLiteral(red: 1, green: 0.9547147289, blue: 0.6170698787, alpha: 1), emojis: ["😃","😂","😍","🥲","😭","😎","😡","😝"]),
    "Food" : Theme(cardColor: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), backgroundColor: #colorLiteral(red: 1, green: 0.9724711441, blue: 0.3258733303, alpha: 1), emojis: ["🍕","🍔","🍟","🌮","🥞","🍝","🍰","🍿"])
  ]
  
  @IBAction func changeTheme(_ sender: Any) {
    if let cvc = splitViewDetailConcentrationViewController {
      if let themeName = (sender as? UIButton)?.currentTitle, let theme = themes[themeName] {
        cvc.theme = theme
      }
    } else if let cvc = lastSeguedToConcentrationViewController {
      if let themeName = (sender as? UIButton)?.currentTitle, let theme = themes[themeName] {
        cvc.theme = theme
      }
      navigationController?.pushViewController(cvc, animated: true)
    }else {
      performSegue(withIdentifier: "Choose Theme", sender: sender)
    }
  }

  // MARK: - Navigation
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "Choose Theme" {
      if let themeName = (sender as? UIButton)?.currentTitle, let theme = themes[themeName] {
        if let cvc = segue.destination as? ConcentrationViewController {
          cvc.randomTheme = theme
          lastSeguedToConcentrationViewController = cvc
        }
      }
    }
  }

}
