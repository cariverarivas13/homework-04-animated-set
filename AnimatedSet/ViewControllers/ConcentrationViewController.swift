//
//  ViewController.swift
//  Concentration
//
//  Created by ApplaudoTest on 6/4/21.
//

import UIKit

class ConcentrationViewController: UIViewController {
    
    //All variables that are been used in the game
  lazy var game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1) / 2)
  var emoji = [Int:String]()
  var emojiChoises: [String] = []
  var startTime = Date()
  @IBOutlet weak var newGameButton: UIButton!
  @IBOutlet weak var flipCountLabel: UILabel!
  @IBOutlet weak var gameScoreLabel: UILabel!
  @IBOutlet var cardButtons: [UIButton]!
  var flipCount = 0 { didSet{ flipCountLabel.text = "Flips: \(flipCount)" } }
  var gameScore = 0 { didSet { gameScoreLabel.text = "Score: \(gameScore)" } }
  var randomTheme = Theme()
  var randomThemeAux = Theme()
  var theme: Theme? {
    didSet{
      randomTheme = theme ?? Theme()
      emoji = [:]
      updateViewFromModel()
      pickTheme()
    }
  }
  
  //Function that is executed when a card is touched, it captures the time when it was touched so it can be used to calculate de gameScore
  //it also checks if the game has ended, and if it has, it shows an alert displaying the score
  @IBAction func touchCard(_ sender: UIButton) {
    let flipTime = Date()
    flipCount = game.flipCount(currentFlips: flipCount)
    
    if let cardNumber = cardButtons.firstIndex(of: sender) {
        
        gameScore = game.chooseCard(at: cardNumber, withStartTime: startTime, cardFlipTime: flipTime)
        
        if game.checkEndGame() {
            let youWon = UIAlertController(title: "You Won!!", message: "You finished the game! \n Score: \(gameScore)", preferredStyle: .alert)
            youWon.addAction(UIAlertAction(title: "Continue", style: .default, handler:{ _ in self.newGame(nil) }))
            self.present(youWon, animated: true, completion: nil)
            
        }
        updateViewFromModel()
    } else {
        print("chosen card was not in cardbuttons")
    }
  }
  
  //With this function we update our views with the information of the model
  func updateViewFromModel() {
    if cardButtons != nil {
      for index in cardButtons.indices {
        let button = cardButtons[index]
        let card = game.cards[index]
        addDecorations(to: button, withColor: UIColor.black, withWidth: 1, withCornerRadius: 5)
        if card.isFaceUp {
            button.backgroundColor = UIColor.white
            button.setTitle(emoji(for: card), for: .normal)
        } else {
            button.setTitle("", for: .normal)
          button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0) : randomTheme.cardColor
            button.layer.borderColor = card.isMatched ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0).cgColor : UIColor.black.cgColor
        }
      }
    }
  }
  
  
  //We override the function to pick our random theme, apply this theme and add decorations to the newGameButton
  override func viewDidLoad() {
      super.viewDidLoad()
    pickTheme()
    updateViewFromModel()
    addDecorations(to: newGameButton, withColor: UIColor.black, withWidth: 1, withCornerRadius: 10)
  }
  
  
  //Function that starts a new game, first we reset our flipCount, gameScore and startTime variables, then we pick a new random theme
  //then we create a new Concentration object and finaly we apply our new theme to our cards
  @IBAction func newGame(_ sender: UIButton?) {
    if sender != nil { animate(this: sender!, withColor: UIColor.black, withDuration: 0.1) }
    flipCount = 0
    gameScore = 0
    startTime = Date()
    randomTheme = randomThemeAux
    pickTheme()
    game = Concentration(numberOfPairsOfCards: (cardButtons.count + 1 ) / 2)
    updateViewFromModel()
  }
  
  
  //Function that picks a random theme and applies the theme to the labels, background and the newGameButton
  func pickTheme() {
    randomThemeAux = randomTheme
    newGameButton.backgroundColor = randomTheme.cardColor
    newGameButton.setTitleColor(randomTheme.backgroundColor, for: .normal)
    gameScoreLabel.textColor = randomTheme.cardColor
    flipCountLabel.textColor = randomTheme.cardColor
    view.backgroundColor = randomTheme.backgroundColor
  }
     
  //function that picks a random emoji for every card
  func emoji(for card: ConcentrationCard) -> String {
      if emoji[card.identifier] == nil, randomTheme.emojis.count > 0 {
          let randomIndex = Int(arc4random_uniform(UInt32(randomTheme.emojis.count)))
          emoji[card.identifier] = randomTheme.emojis.remove(at: randomIndex)
      }
      return emoji[card.identifier] ?? "?"
  }
  
  
  //Function that adds a cornerRadius, border and shadow to a button
  func addDecorations(to button: UIButton, withColor color: UIColor, withWidth width: Int, withCornerRadius cornerRadius: Int) {
      
      button.layer.cornerRadius = CGFloat(cornerRadius)
      button.layer.borderWidth = CGFloat(width)
      button.layer.borderColor = color.cgColor
      button.layer.shadowColor = UIColor.gray.cgColor
      button.layer.shadowOffset = CGSize(width: 0, height: 2.0)
      button.layer.shadowOpacity = 0.5
      button.layer.shadowRadius = 0.0
  }
      
  
  //Function that animates a button
  func animate(this button: UIButton, withColor color: UIColor, withDuration duration: Double) {
      
      let newAnimation = CABasicAnimation(keyPath: "backgroundColor")
      newAnimation.fromValue = color.cgColor
      newAnimation.duration = duration
      button.layer.add(newAnimation, forKey: "ColorPulse")
      
  }
  
}
