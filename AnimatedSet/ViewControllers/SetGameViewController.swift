//
//  ViewController.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 28/4/21.
//

import UIKit

class SetGameViewController: UIViewController {

  //All variables that are been used in the game
  private lazy var game = SetGame(numberOfCards: 81)
  private(set) var cardsAtPlay: [Cards] = []
  private var cardsToAddVC: [Cards] = []
  private var animationIsRunning: Timer?
  private var amountOfTime = 0.0
  private var isThereASet = 0
  private var gameScore = 0
  private var playerTurn = 0
  @IBOutlet private weak var addThreeCardsButton: UIButton!
  @IBOutlet weak var scoreLabel: UILabel!
  @IBOutlet weak var newGameButton: UIButton!
  @IBOutlet weak var deckButton: UIButton!
  @IBOutlet weak var setButton: UIButton!
  @IBOutlet weak var buttonStackView: UIStackView!
  private var isDeckEmpty = false
  private var deckButtonPoint = CGPoint()
  private var setButtonPoint = CGPoint()
  //Gesture recognizer for que rotation gesture, we added it to the view so it can be donde in the whole screen
  override var view: UIView! {
    didSet {
      let rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotationGestureShuffle))
      view.addGestureRecognizer(rotationGesture)
    }
  }
  
  //Gesture recognizers for the tap gesture and the swipe down gesture
  @IBOutlet weak var cardsSuperview: DeckGridView! {
    didSet {
      let tap = UITapGestureRecognizer(target: self, action: #selector(findTappedCard))
      tap.numberOfTapsRequired = 1
      cardsSuperview.addGestureRecognizer(tap)
    }
  }
  
  //We override viewDidLoad to initialize our cardsAtPlay and to add decorations to our newGameButton and addThreeCardsButton
  override func viewDidLoad() {
    super.viewDidLoad()
    cardsAtPlay = game.startGame()
    addDecorations(to: addThreeCardsButton, withBorderWidth: 3, withBorderColor: #colorLiteral(red: 0.3582406938, green: 0.237229526, blue: 0.5366020799, alpha: 1))
    addDecorations(to: newGameButton, withBorderWidth: 3, withBorderColor: #colorLiteral(red: 0.3582406938, green: 0.237229526, blue: 0.5366020799, alpha: 1))
    addDecorations(to: deckButton, withBorderWidth: 3, withBorderColor: #colorLiteral(red: 0.5897196531, green: 0.7979089618, blue: 0.7327213883, alpha: 1))
    addDecorations(to: setButton, withBorderWidth: 3, withBorderColor: #colorLiteral(red: 0.5897196531, green: 0.7979089618, blue: 0.7327213883, alpha: 1))
    viewWillLayoutSubviews()
  }
  
  //we override viewDidAppear to update the location of our deck button and set deck button, and to animate and show
  //the initial cards of the game
  override func viewDidAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    updateDeckAndSetLocationsInView()
    cardsSuperview.createLayoutsForCurrentCards(forCards: cardsAtPlay)
  }
  
  //function that updates the CGFrame of our deck button and set deck button when the screen orientation changes
  private func updateDeckAndSetLocationsInView(){
    deckButtonPoint = buttonStackView.convert( CGPoint(x: deckButton.frame.minX, y: deckButton.frame.minY), to: cardsSuperview)
    cardsSuperview.updateDeckLocation(withFrame:
                                        CGRect(x: deckButtonPoint.x,
                                               y: deckButtonPoint.y,
                                               width: buttonStackView.bounds.width * 0.333 - 5,
                                               height: buttonStackView.bounds.height))
    setButtonPoint = buttonStackView.convert( CGPoint(x: setButton.frame.minX, y: setButton.frame.minY), to: cardsSuperview)
    cardsSuperview.updateSetDeckLocation(withFrame:
                                          CGRect(x: setButtonPoint.x,
                                                 y: setButtonPoint.y,
                                                 width: buttonStackView.bounds.width * 0.333 - 5,
                                                 height: buttonStackView.bounds.height))
  }
  
  //we override viewDisLayoutSubviews to be update the bounds of our superview and to update de frame of our deck and
  //set buttons
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    updateDeckAndSetLocationsInView()
    cardsSuperview.changeGridFrame(withNewBounds: self.cardsSuperview.bounds)
  }
  
  //Function that gets the point where the user has touched and finds the corresponding card on that point
  //when three cards have been picked it checks with the model to see if a set has been made
  @objc func findTappedCard(_ sender: UITapGestureRecognizer) {
    if !(animationIsRunning?.isValid ?? false) {
      let locationOfTap: CGPoint = sender.location(in: sender.view)
      let indexOfCard = cardsSuperview.aCardsIsPicked(inLocation: locationOfTap)
      isThereASet = 0
      if indexOfCard != -1 {
        isThereASet = game.pickCard(at: indexOfCard)
        if isThereASet == 1 {
          calculatePoints(withMessage: "Is a set!!", wasSet: 1)
          updateCards(type: 0)
        } else if isThereASet == 2 {
          calculatePoints(withMessage: "Is not a set!!", wasSet: 2)
          game.checkIfThereIsASet()
          updateCards(type: 2)
          cardsSuperview.gameStatus = EnumGameStatus.pickCard
          cardsSuperview.createLayoutsForCurrentCards(forCards: cardsAtPlay)
        } else if isThereASet == 0 {
          updateCards(type: 2)
          cardsSuperview.gameStatus = EnumGameStatus.pickCard
          cardsSuperview.createLayoutsForCurrentCards(forCards: cardsAtPlay)
        }
      } else {
          print("choosen card was not in cardbuttons")
      }
    } else {
      self.showToast(withMessage: "An animation is running!", withFont: .systemFont(ofSize: 15.0))
    }
  }
  
  //function that add three more cards to our cardsAtPlay, it also informs the user if the deckOfCards is empty
  @objc @IBAction private func addThreeMoreCards() {
    if game.deckOfCards.isEmpty{
        self.showToast(withMessage: "The deck is empty!!", withFont: .systemFont(ofSize: 15.0))
    } else {
      updateCards(type: 1)
    }
  }
  
  //function that calls the model to shuffle the cards when the rotation gesture has been made and calls the animations
  //to show it to the user
  @objc private func rotationGestureShuffle(_ sender: UIRotationGestureRecognizer){
    if sender.state == .ended {
      amountOfTime = 3.2
      game.shuffleCards()
      cardsAtPlay.removeAll()
      cardsAtPlay = game.cardsAtPlay
      cardsSuperview.gameStatus = EnumGameStatus.shuffleCardsFlyingToDeck
      cardsSuperview.shuffleLayoutsForCurrentCards(forCards: cardsAtPlay)
      Timer.scheduledTimer(withTimeInterval: 0.6, repeats: false) { [weak self] (timer) in
        self?.cardsSuperview.gameStatus = EnumGameStatus.shuffleCardsExplotion
        self?.cardsSuperview.shuffleLayoutsForCurrentCards(forCards: self!.cardsAtPlay)
        timer.invalidate()
      }
      Timer.scheduledTimer(withTimeInterval: 2.6, repeats: false) { [weak self] (timer) in
        self?.cardsSuperview.gameStatus = EnumGameStatus.shuffleCardsToPosition
        self?.cardsSuperview.shuffleLayoutsForCurrentCards(forCards: self!.cardsAtPlay)
      }
      self.showToast(withMessage: "Shuffle!!", withFont: .systemFont(ofSize: 15.0))
      animationIsRunning = Timer.scheduledTimer(withTimeInterval: amountOfTime, repeats: false, block: { [weak self] (timer) in
        self?.amountOfTime = 0
        timer.invalidate()
      })
    }
  }
  
  //function to calculate the score of the players
  private func calculatePoints(withMessage message: String, wasSet: Int){
    self.showToast(withMessage: message, withFont: .systemFont(ofSize: 15.0))
      gameScore = game.getGameScore(gameScore, wasSet)
      scoreLabel.text = "Score: \(gameScore)"
  }
  
  //function that updates the cards depending on the type, and will do the corresponding animations,
  //type 0 wil check if there is a set and type 3 will add 3 more cards
  private func updateCards(type: Int){
    isDeckEmpty = (game.deckOfCards.isEmpty) ? true : false
    if type == 0 {
      amountOfTime = 3.8
      game.checkIfThereIsASet()
      cardsSuperview.gameStatus = EnumGameStatus.removeSetFlying
      cardsSuperview.removeLayoutsForSetCards(withIndices: game.indicesOfCardsToRepleaceModel)
      Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { [unowned self] timer in
        cardsSuperview.gameStatus = EnumGameStatus.removeSetToDeck
        cardsSuperview.removeLayoutsForSetCards(withIndices: game.indicesOfCardsToRepleaceModel)
        timer.invalidate()
      }
      Timer.scheduledTimer(withTimeInterval: 3.7, repeats: false) { [unowned self] timer in
        if !isDeckEmpty{
          cardsSuperview.gameStatus = EnumGameStatus.replaceSet
          cardsSuperview.createLayoutsForReplacedCards(forCards: game.cardsToAdd, withIndices: game.indicesOfCardsToRepleaceModel)
        } else {
          cardsSuperview.gameStatus = EnumGameStatus.replaceSetEndGame
          cardsSuperview.removeEndGameLayoutForSetCards(withIndices: game.indicesOfCardsToRepleaceModel)
        }
        timer.invalidate()
      }
    }
    if type == 1 {
      amountOfTime = Double(cardsAtPlay.count) * 0.03
      game.checkIfThereIsASet()
      cardsSuperview.gameStatus = .addThreeCards
      cardsSuperview.createLayoutsForAddedCards(forCards: game.getCards(amount: 3))
    }
    cardsAtPlay.removeAll()
    cardsAtPlay = game.cardsAtPlay
    animationIsRunning = Timer.scheduledTimer(withTimeInterval: amountOfTime, repeats: false, block: { [weak self] (timer) in
      self?.amountOfTime = 0
      timer.invalidate()
    })
  }
  
  //function that starts a new game, it creates a new object of SetGame and resets both players score and cardsAtPlay array,
  //also gets the new cardsAtPlay array and shows it to the user
  @IBAction private func startNewGame() {
    game = SetGame(numberOfCards: 81)
    isDeckEmpty = false
    gameScore = 0
    scoreLabel.text = "Score: \(game.getGameScore(0,0))"
    cardsAtPlay.removeAll()
    cardsAtPlay = game.startGame()
    cardsSuperview.gameStatus = EnumGameStatus.startGame
    cardsSuperview.createLayoutsForCurrentCards(forCards: cardsAtPlay)
  }
  
  //function that add a cornerRadius and a border with a certain width and color
  private func addDecorations(to button: UIButton, withBorderWidth width: Int, withBorderColor colorBorder: UIColor) {
      button.layer.borderWidth = CGFloat(width)
      button.layer.borderColor = colorBorder.cgColor
      button.layer.cornerRadius = 10
  }
}

//MARK: Extension to show a toast to the user
extension UIViewController {
  func showToast(withMessage message: String, withFont font: UIFont) {
      let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height-150, width: 250, height: 35))
      toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
      toastLabel.textColor = UIColor.white
      toastLabel.font = font
      toastLabel.textAlignment = .center;
      toastLabel.text = message
      toastLabel.alpha = 1.0
      toastLabel.layer.cornerRadius = 10;
      toastLabel.clipsToBounds  =  true
      self.view.addSubview(toastLabel)
      UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
           toastLabel.alpha = 0.0
      }, completion: {(isCompleted) in
          toastLabel.removeFromSuperview()
      })
  }

}

