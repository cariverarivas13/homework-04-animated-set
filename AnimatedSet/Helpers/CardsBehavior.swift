//
//  CardsBehavior.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 30/4/21.
//

import UIKit

class CardsBehavior: UIDynamicBehavior {
  
  //we define the collision behavior that our items will have
  lazy var collisionBehavior: UICollisionBehavior = {
    let behavior = UICollisionBehavior()
    behavior.translatesReferenceBoundsIntoBoundary = true
    behavior.collisionMode = .boundaries
    return behavior
  } ()
  
  //we define the item behaviour that our items will have
  lazy var itemBehavior: UIDynamicItemBehavior = {
    let behavior = UIDynamicItemBehavior()
    behavior.allowsRotation = false
    behavior.density = 0
    behavior.elasticity = 0.8
    behavior.friction = 0
    behavior.resistance = 0
    return behavior
  } ()
  
  //function that pushes our items with a certain magnitude and random angle
  private func push(_ item: UIDynamicItem) {
    let push = UIPushBehavior(items: [item], mode: .instantaneous)
    if let referenceBounds = dynamicAnimator?.referenceView?.bounds {
      let center = CGPoint(x: referenceBounds.midX, y: referenceBounds.midY)
      push.angle = (CGFloat.pi/2).arc4random
      switch (item.center.x, item.center.y) {
      case let (x, y) where x < center.x && y > center.y:
        push.angle = -1 * push.angle
      case let (x, y) where x > center.x:
        push.angle = y < center.y ? CGFloat.pi-push.angle: CGFloat.pi+push.angle
      default:
        push.angle = (CGFloat.pi*2).arc4random
      }
    }
  push.magnitude = CGFloat(0.7) + CGFloat(1.0).arc4random
    push.action = { [unowned push, weak self] in
      self?.removeChildBehavior(push)
    }
    addChildBehavior(push)
  }

  //function that will add a new item to have all the behaviors
  func addItem(_ item: UIDynamicItem) {
    collisionBehavior.addItem(item)
    itemBehavior.addItem(item)
    push(item)
  }
   
  //funtion that will remove an item from the behaviors
  func removeItem(_ item: UIDynamicItem) {
    collisionBehavior.removeItem(item)
    itemBehavior.removeItem(item)
  }
  
  //initialiizer
  override init() {
    super.init()
    addChildBehavior(collisionBehavior)
    addChildBehavior(itemBehavior)
  }
  
  //convinience initializer
  convenience init(in animator: UIDynamicAnimator) {
    self.init()
    animator.addBehavior(self)
  }

}

// MARK: extension to create a random CGFloat
extension CGFloat {
    var arc4random: CGFloat {
        return self * (CGFloat(arc4random_uniform(UInt32.max))/CGFloat(UInt32.max))
    }
}
