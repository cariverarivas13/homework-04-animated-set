//
//  Theme.swift
//  Concentration
//
//  Created by ApplaudoTest on 7/4/21.
//
// This struct is a helper that allow us to create Theme objects to apply them to our UI, this objects need to have a card color, background color and an array of a
// minimum of 8 emojis

import UIKit

struct Theme {
    
  var cardColor: UIColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
  var backgroundColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
  var emojis: [String] = ["👹", "👻", "💀", "🎃", "🦇", "🕷", "🔮", "😈"]
  
}
