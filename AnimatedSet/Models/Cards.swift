//
//  Cards.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 28/4/21.
//

import Foundation

struct Cards: Hashable, Equatable {
    
  var isPicked = false
  var amountOfSymbols = 0
  var shapeType = ""
  var shadingType = 0
  var color = ""
  
  private(set) var identifier: Int
  
  static func == (lhs: Cards, rhs: Cards) -> Bool{
    return lhs.identifier == rhs.identifier
  }
    
  private static var identifierFactory = 0
  
  private static func getUniqueIdentifier() -> Int {
      identifierFactory += 1
      return identifierFactory
  }
  
  init() {
      self.identifier = Cards.getUniqueIdentifier()
  }
    
}
