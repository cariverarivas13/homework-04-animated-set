//
//  Concentration.swift
//  Concentration
//
//  Created by ApplaudoTest on 6/4/21.
//
// Model Concentration

import Foundation

class Concentration {
    
    var cards = [ConcentrationCard]()
    var indexOfOneAndOnlyFaceUpCard: Int?
    var cardFlips = 0
    var gamePoints = 0
    
    //Init of the class, it creates all the cards that are going to be used, shuffles the order and resets the gamePoints and cardFlips
    init(numberOfPairsOfCards: Int) {
        for _ in 1...numberOfPairsOfCards {
            let card = ConcentrationCard()
            cards += [card, card]
        }
        cardFlips = 0
        gamePoints = 0
        cards.shuffle()
    }
    
    //Logic of the game, it flips the picked cards and compares if the cards have the same emoji, it adds or deducts points depending if the match was correct or not
    //and the time that it took to make the match
    func chooseCard(at index: Int, withStartTime startTime: Date, cardFlipTime: Date) -> Int {
        let timeInterval = Int(cardFlipTime.timeIntervalSince(startTime))
                
        if !cards[index].isMatched {
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                if cards[matchIndex].identifier == cards[index].identifier {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                    gamePoints = gamePoints + 2 + calculateBonusPoints(withTime: timeInterval)
                } else {
                    if cards[matchIndex].wasSeen && cards[index].wasSeen {
                        gamePoints = gamePoints - 2 - calculateDecreasePoints(withTime: timeInterval)
                        
                    }
                    else if cards[matchIndex].wasSeen || cards[index].wasSeen {
                        gamePoints = gamePoints - 1 - calculateDecreasePoints(withTime: timeInterval)
                        
                    }
                }
                cards[index].isFaceUp = true
                cards[matchIndex].wasSeen = true
                cards[index].wasSeen = true
                indexOfOneAndOnlyFaceUpCard = nil
            } else {
                for flipDownIndex in cards.indices {
                    cards[flipDownIndex].isFaceUp = false
                }
                cards[index].isFaceUp = true
                indexOfOneAndOnlyFaceUpCard = index
            }
        }
        return gamePoints
    }
    
    //Funtion that tracks the amount of flips the user has made
    func flipCount(currentFlips flips: Int) -> Int { return flips + 1 }
    
    //Funtion that modifies the score, it adds 4 bonus points at the beggining, and over time add less bonus points, until it just returns 0 bonus points
    func calculateBonusPoints(withTime time: Int) -> Int {
        let bonus = -( (time) / 2) + 20
        if bonus <= 0 { return 0 }
        else { return Int(Double(bonus).squareRoot()) }
    }
  
    //Function that modifies the score, it substracts one point every 12 seconds
    func calculateDecreasePoints(withTime time: Int) -> Int {
        let decrease = time / 12
        return decrease
    }

    //Function that checks if all cards have been matched, and if the have, finishes the game
    func checkEndGame() -> Bool {
        for index in cards.indices {
            if !cards[index].isMatched {
                return false
                
            }
        }
        return true
    }
    
}















