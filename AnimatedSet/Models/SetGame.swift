//
//  SetGame.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 28/4/21.
//

import Foundation

class SetGame {
  
  private(set) var deckOfCards: [Cards] = []
  private(set) var cardsAtPlay: [Cards] = []
  private(set) var indicesOfCardsToRepleaceModel: [Int] = []
  private(set) var cardsToAdd: [Cards] = []
  private var cardsCurrentlyPicked = 0
  private(set) var isThereASetModel = false
  private var setOfNumbers = Set<Int>()
  private var setOfShapes = Set<String>()
  private var setOfShading = Set<Int>()
  private var setOfColors = Set<String>()
  
  //closure that returns the score of the game
  var getGameScore: (Int, Int)->(Int) = { score, type  in
      var gameScore = 0
      switch type {
      case 1: gameScore = score + 10
      case 2: gameScore = score - 5
      default:
          return 0
      }
      return gameScore
  }
  
  //Init of the struct, it resets the deckOfCards, cardsAtPlay and the cardsCurrentlyPicked to make sure
  //they're empty, it creates the new deckOfCards and calls the method to give each card their properties
  //it finally shuffles the cards
  init(numberOfCards: Int) {
    deckOfCards.removeAll()
    cardsAtPlay.removeAll()
    for _ in 1...numberOfCards {
      var card = Cards()
      card.isPicked = false
      deckOfCards.append(card)
    }
    addSetPropertiesToCards()
    cardsCurrentlyPicked = 0
    deckOfCards.shuffle()
  }
  
  //function to start a new game with 12 cardsAtPlay
  func startGame() -> [Cards]{
    cardsAtPlay.removeAll()
    for _ in 0..<12 {
        cardsAtPlay.append(deckOfCards.removeFirst())
    }
    return cardsAtPlay
  }
  
  //function that add the specified amount of cards to the cardsAtPlay
  func getCards(amount addedCards: Int) -> [Cards]{
    cardsToAdd.removeAll()
    for _ in 0..<addedCards {
        cardsToAdd.append(deckOfCards.removeFirst())
    }
    cardsAtPlay.append(contentsOf: cardsToAdd)
    return cardsToAdd
  }

  //function that shuffles all the current cards at play
  func shuffleCards(){
    cardsAtPlay.shuffle()
  }
  
  //function that checks how many cards have been picked, depending on the amount, it calls the function checkIfThereIsASet
  //to see if a set has been made, also it changes the property isPicked of the card to true, if the card has been picked
  //before, then changes the property isPicked to false.
  func pickCard(at index: Int) -> Int {
    if index > cardsAtPlay.count { return 0 }
    if index < cardsAtPlay.count, cardsAtPlay[index].isPicked {
        cardsAtPlay[index].isPicked = false
        cardsCurrentlyPicked -= 1
    } else if index < cardsAtPlay.count, !(cardsAtPlay[index].isPicked){
        cardsAtPlay[index].isPicked = true
        cardsCurrentlyPicked += 1
        if cardsCurrentlyPicked == 3 {
            if analizeSet() {
              isThereASetModel = true
              return 1
            }
            else {
              isThereASetModel = false
              return 2
            }
        }
    }
    return 0
  }
  
  //function that checks if a set has been made, if it has, it calls removeSetCards to remove the cards.
  //if it wasn't a set then it just changes the property isPicked of all cards to false
  func checkIfThereIsASet(){
    if isThereASetModel {
        removeSetCards()
        isThereASetModel = false
    }
    for index in cardsAtPlay.indices {
        cardsAtPlay[index].isPicked = false
    }
    cardsCurrentlyPicked = 0
  }
  
  //function that filters the selected cards index and then either replaces them with a new card
  //or if the deckOfCards is empty, sets the property isMatched to true
  func removeSetCards(){
    var selectedCards = cardsAtPlay.indices.filter { cardsAtPlay[$0].isPicked }
    selectedCards.sort()
    selectedCards.reverse()
    cardsToAdd.removeAll()
    indicesOfCardsToRepleaceModel.removeAll()
    for selectedIndex in selectedCards {
      if deckOfCards.isEmpty {
        indicesOfCardsToRepleaceModel.append(selectedIndex)
        cardsAtPlay.remove(at: selectedIndex)
      } else {
        indicesOfCardsToRepleaceModel.append(selectedIndex)
        cardsToAdd.append(deckOfCards.first!)
        cardsAtPlay[selectedIndex] = deckOfCards.removeFirst()
      }
    }
  }
  
  //function that gives each card their unique properties
  func addSetPropertiesToCards(){
    var cardCount = 0
    for number in (1...3){
        for shape in (1...3){
            for shading in (1...3){
                for color in (1...3){
                    switch number {
                    case 1: deckOfCards[cardCount].amountOfSymbols = EnumProperties.AmountOfCards.amountOne.rawValue
                    case 2: deckOfCards[cardCount].amountOfSymbols = EnumProperties.AmountOfCards.amountTwo.rawValue
                    case 3: deckOfCards[cardCount].amountOfSymbols = EnumProperties.AmountOfCards.amountThree.rawValue
                    default:
                        deckOfCards[cardCount].amountOfSymbols = 0
                    }
                    switch shape {
                    case 1: deckOfCards[cardCount].shapeType = EnumProperties.TypeOfShape.typeDiamond.rawValue
                    case 2: deckOfCards[cardCount].shapeType = EnumProperties.TypeOfShape.typeOval.rawValue
                    case 3: deckOfCards[cardCount].shapeType = EnumProperties.TypeOfShape.typeSquiggle.rawValue
                    default:
                        deckOfCards[cardCount].shapeType = ""
                    }
                    switch shading {
                    case 1: deckOfCards[cardCount].shadingType = EnumProperties.TypeOfShading.typeFill.rawValue
                    case 2: deckOfCards[cardCount].shadingType = EnumProperties.TypeOfShading.typeBorder.rawValue
                    case 3: deckOfCards[cardCount].shadingType = EnumProperties.TypeOfShading.typeStripes.rawValue
                    default:
                        deckOfCards[cardCount].shadingType = 0
                    }
                    switch color {
                    case 1: deckOfCards[cardCount].color = EnumProperties.TypeOfColor.colorCyan.rawValue
                    case 2: deckOfCards[cardCount].color = EnumProperties.TypeOfColor.colorRed.rawValue
                    case 3: deckOfCards[cardCount].color = EnumProperties.TypeOfColor.colorPurple.rawValue
                    default:
                        deckOfCards[cardCount].color = ""
                    }
                    cardCount += 1
                }
            }
        }
    }
  }
  
  //function that fills the sets for each property and analize them to see if there was a set or not
  func analizeSet() -> Bool{
    for index in (cardsAtPlay.indices){
        if cardsAtPlay[index].isPicked{
            setOfNumbers.insert(cardsAtPlay[index].amountOfSymbols)
            setOfShapes.insert(cardsAtPlay[index].shapeType)
            setOfShading.insert(cardsAtPlay[index].shadingType)
            setOfColors.insert(cardsAtPlay[index].color)
        }
    }
    if setOfNumbers.count == 3 || setOfNumbers.count == 1 ,
       setOfShapes.count == 3 || setOfShapes.count == 1 ,
       setOfShading.count == 3 || setOfShading.count == 1 ,
       setOfColors.count == 3 || setOfColors.count == 1 {
        resetSets()
        return true
    }else{
        resetSets()
        return false
    }
  }
  
  //resets the sets
  func resetSets(){
    setOfNumbers.removeAll()
    setOfShapes.removeAll()
    setOfShading.removeAll()
    setOfColors.removeAll()
  }
    
}
