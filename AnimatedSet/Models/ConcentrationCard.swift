//
//  Card.swift
//  Concentration
//
//  Created by ApplaudoTest on 6/4/21.
//
// Struct that is the model of the cards, here the cards can choose an unique identifier and have all the properties that are needed to work correctly
import Foundation

struct ConcentrationCard {
    
    var isFaceUp = false
    var isMatched = false
    var wasSeen = false
    var identifier: Int
    
    static var identifierFactory = 0
    
    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    init() {
        self.identifier = ConcentrationCard.getUniqueIdentifier()
    }
    
}
