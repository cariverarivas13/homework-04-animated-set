//
//  DeckGridView.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 28/4/21.
//

import UIKit

class DeckGridView: UIView {

  private lazy var gridDeck = Grid(layout: .aspectRatio(0.8), frame: bounds)
  private lazy var animator = UIDynamicAnimator(referenceView: self)
  private lazy var cardBehavior = CardsBehavior(in: animator)
  private var indicesOfCardsToReplace: [Int] = []
  private var cardsAtPlayView = [Cards]()
  private var layoutForCards = [CardView]()
  private var deckButtonProperties = CGRect()
  private var setButtonProperties = CGRect()
  var gameStatus: EnumGameStatus = .startGame
  
  //function that depending on the status of the game will call the corresponding function
  override func layoutSubviews() {
    super.layoutSubviews()
    switch gameStatus {
    case .startGame: showCards(withDelay: 0.1)
    case .addThreeCards, .gameIdle, .replaceSetEndGame:
      showCards(withDelay: 0.02)
    case .pickCard: showNoAnimation()
    case .removeSetFlying: removeLayoutSetCards()
    case .removeSetToDeck: removeLayoutSetCards()
    case .shuffleCardsExplotion, .shuffleCardsFlyingToDeck, .shuffleCardsToPosition:
      shuffleTheCards()
    case .replaceSet: replaceSetCards()
    case .idle: break
    }
  }
  
  //Function that shows the new frames that the grid struct creates with no animation
  private func showNoAnimation(){
    gridDeck.cellCount = layoutForCards.count
    for index in (0 ..< layoutForCards.count) {
      if let frame = gridDeck[index] {
        layoutForCards[index].isFaceUp = true
        layoutForCards[index].frame = CGRect(
          x: frame.minX,
          y: frame.minY,
          width: frame.width - 5,
          height: frame.height - 5)
        self.addSubview(layoutForCards[index])
      }
    }
    gameStatus = .gameIdle
  }
  
  //Function that shows all our cards and animates them to their correct position, if the card is face down it also flips them over so the
  //user can see them
  private func showCards(withDelay delay: Double){
    gridDeck.cellCount = layoutForCards.count
    for index in (0 ..< layoutForCards.count) {
      if let frame = gridDeck[index] {
        if !layoutForCards[index].isFaceUp {
          UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.2,
            delay: TimeInterval(Double(index) * delay),
            options: [],
            animations: { [unowned self] in
              self.layoutForCards[index].alpha = 1.0
              self.layoutForCards[index].frame = CGRect(
                x: frame.minX,
                y: frame.minY,
                width: frame.width - 5,
                height: frame.height - 5)
            }) { [unowned self] (UIViewAnimatingPosition) in
            UIView.transition(
              with: layoutForCards[index],
              duration: 0.4,
              options: [.transitionFlipFromLeft],
              animations:{ layoutForCards[index].isFaceUp = true })
          }
        } else {
          UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.2,
            delay: TimeInterval(Double(index) * delay),
            options: [],
            animations: { [unowned self] in
              self.layoutForCards[index].alpha = 1.0
              self.layoutForCards[index].frame = CGRect(
                x: frame.minX,
                y: frame.minY,
                width: frame.width - 5,
                height: frame.height - 5)
            })
        }
        self.addSubview(layoutForCards[index])
      }
    }
  }
  
  //function that animates the frames for the set cards according to the gameStatus, if the game status is removeSetFlying it makes the set cards
  //fly across the screen bouncing around, if the game status is removeSetToDeck it makes the cards fly to the Set Deck
  private func removeLayoutSetCards(){
    gridDeck.cellCount = layoutForCards.count
    var delayFix = 0
    for index in (0 ..< layoutForCards.count) {
      if let frame = gridDeck[index] {
        if gameStatus == EnumGameStatus.removeSetFlying, indicesOfCardsToReplace.contains(index){
          layoutForCards[index].layer.zPosition = 10
          cardBehavior.addItem(layoutForCards[index])
        } else if gameStatus == EnumGameStatus.removeSetToDeck, indicesOfCardsToReplace.contains(index) {
          cardBehavior.removeItem(layoutForCards[index])
          UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.5,
            delay: TimeInterval(Double(delayFix) * 0.2),
            options: [.curveEaseInOut, .transitionFlipFromLeft],
            animations: { [unowned self] in
              self.layoutForCards[index].layer.zPosition = 10
              self.layoutForCards[index].frame = setButtonProperties
              self.layoutForCards[index].transform = CGAffineTransform(rotationAngle: CGFloat.pi)
              self.layoutForCards[index].alpha = 0
            }
          )
          delayFix += 1
        } else {
          layoutForCards[index].frame = CGRect(
            x: frame.minX,
            y: frame.minY,
            width: frame.width - 5,
            height: frame.height - 5)
        }
        self.addSubview(layoutForCards[index])
      }
    }
    delayFix = 0
  }
  
  //function that animates the new frames that our grid class creates for our views, it makes the cards to be added fly from the deck to their
  //position on the grid and then flips them over for the user to see
  private func replaceSetCards(){
    gridDeck.cellCount = layoutForCards.count
    var delayFix = 0
    for index in (0 ..< layoutForCards.count) {
      if let frame = gridDeck[index] {
        if indicesOfCardsToReplace.contains(index){
          layoutForCards[index].alpha = 1
          UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.2,
            delay: TimeInterval(Double(delayFix) * 0.2),
            options: [.curveEaseInOut],
            animations: { [unowned self] in
              self.layoutForCards[index].layer.zPosition = 5
              self.layoutForCards[index].frame = CGRect(
                x: frame.minX,
                y: frame.minY,
                width: frame.width - 5,
                height: frame.height - 5)
            }) { [unowned self] (UIViewAnimatingPosition) in
                UIView.transition(
                  with: layoutForCards[index],
                  duration: 0.4,
                  options: [.transitionFlipFromLeft],
                  animations:{ layoutForCards[index].isFaceUp = true })
          }
          delayFix += 1
        } else {
          layoutForCards[index].frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width - 5, height: frame.height - 5)
        }
        self.addSubview(layoutForCards[index])
      }
    }
    delayFix = 0
    gameStatus = .gameIdle
  }
  
  //function that makes all the animations for our shuffled cards
  private func shuffleTheCards(){
    gridDeck.cellCount = layoutForCards.count
    for index in (0..<layoutForCards.count) {
      if let frame = gridDeck[index] {
        if gameStatus == .shuffleCardsFlyingToDeck {
          UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.5,
            delay: 0,
            options: [.curveEaseInOut],
            animations: { [unowned self] in
              self.layoutForCards[index].layer.zPosition = 10
              self.layoutForCards[index].frame = CGRect(
                x: self.center.x - frame.width / 2,
                y: self.center.y - frame.height / 2,
                width: frame.width / 2,
                height: frame.height / 2)
              self.layoutForCards[index].transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            })
          self.addSubview(layoutForCards[index])
        } else if gameStatus == .shuffleCardsExplotion {
          self.addSubview(layoutForCards[index])
          cardBehavior.addItem(layoutForCards[index])
        } else if gameStatus == .shuffleCardsToPosition {
          cardBehavior.removeItem(layoutForCards[index])
          UIViewPropertyAnimator.runningPropertyAnimator(
            withDuration: 0.5,
            delay: 0.2,
            options: [.curveEaseInOut],
            animations: { [unowned self] in
              self.layoutForCards[index].layer.zPosition = 1
              self.layoutForCards[index].frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width - 5, height: frame.height - 5)
            })
          self.addSubview(layoutForCards[index])
        }
      }
    }
  }
  
  //Function that creates new views for all our current cards at play
  func createLayoutsForCurrentCards(forCards cards: [Cards]){
    cardsAtPlayView.removeAll()
    cardsAtPlayView = cards
    layoutForCards.removeAll()
    self.subviews.forEach({ $0.removeFromSuperview() })
    for cardIndex in cards.indices {
      layoutForCards.append(CardView(frame: deckButtonProperties, card: cardsAtPlayView[cardIndex]))
      if gameStatus == .startGame { layoutForCards[cardIndex].alpha = 0.0}
      layoutForCards[cardIndex].isFaceUp = false
      layoutForCards[cardIndex].contentMode = .redraw
    }
    setNeeds()
    layoutIfNeeded()
  }
  
  //Function that creates new views when we add 3 new cards
  func createLayoutsForAddedCards(forCards cards: [Cards]) {
    cardsAtPlayView.append(contentsOf: cards)
    for cardIndex in cards.indices{
      layoutForCards.append(CardView(frame: deckButtonProperties, card: cards[cardIndex]))
      layoutForCards[cardIndex].isFaceUp = true
      layoutForCards[cardIndex].layer.zPosition = 1.0
      layoutForCards[cardIndex].contentMode = .redraw
    }
    setNeeds()
  }
  
  //function that updates the indices of the cards that have to be removed when a set has been made
  func removeLayoutsForSetCards(withIndices indices: [Int]) {
    indicesOfCardsToReplace = indices
    setNeeds()
    layoutIfNeeded()
  }
  
  //Function that removes the layouts of the set cards when there are no more cards on the deck
  func removeEndGameLayoutForSetCards(withIndices indices: [Int]) {
    indicesOfCardsToReplace = indices
    for cardIndex in indices.indices {
      layoutForCards.remove(at: indices[cardIndex])
    }
    setNeeds()
    layoutIfNeeded()
  }
  
  //Function that creates new views for the cards that are gonna be added to replace the set cards
  func createLayoutsForReplacedCards(forCards cards: [Cards], withIndices indices: [Int]) {
    indicesOfCardsToReplace = indices
    for cardIndex in cards.indices {
      layoutForCards[indices[cardIndex]] = CardView(frame: deckButtonProperties, card: cards[cardIndex])
      layoutForCards[indices[cardIndex]].isFaceUp = false
      layoutForCards[indices[cardIndex]].contentMode = .redraw
      cardsAtPlayView[indices[cardIndex]] = cards[cardIndex]
    }
    setNeeds()
  }
  
  //function that updates the views of the cards that are going to be shuffled
  func shuffleLayoutsForCurrentCards(forCards cards: [Cards]) {
    self.subviews.forEach({ $0.removeFromSuperview() })
    for cardIndex in cards.indices {
      layoutForCards[cardIndex] = CardView(
        frame: layoutForCards[cardIndex].frame,
        card: cards[cardIndex])
      layoutForCards[cardIndex].isFaceUp = true
      layoutForCards[cardIndex].contentMode = .redraw
    }
    cardsAtPlayView.removeAll()
    cardsAtPlayView = cards
    setNeeds()
  }
  
  //function that changes the bounds of our grid to correctly display the cards in portrait and landscape mode, we use again setNeedsDisplay to inform
  //that the rect needs to be redrawn
  func changeGridFrame(withNewBounds newBounds: CGRect){
    gridDeck.frame = newBounds
    setNeedsDisplay()
  }
  
  //Updates the frame for our deck when the screen rotates
  func updateDeckLocation(withFrame newFrame: CGRect) {
    deckButtonProperties = newFrame
    setNeeds()
  }
  
  //Updates the frame for our set deck when the screen rotates
  func updateSetDeckLocation(withFrame newFrame: CGRect) {
    setButtonProperties = newFrame
    setNeeds()
  }
  
  //Function that will call setNeedsDisplay and setNeedsLayout when we need them
  func setNeeds(){
    setNeedsDisplay()
    setNeedsLayout()
  }
  
  //function that finds the exact frame of the card that has been tapped with the information sent by the viewcontroller and returns the index of that card
  func aCardsIsPicked(inLocation location: CGPoint) -> Int{
    for index in (0..<layoutForCards.count) {
      if let frame = gridDeck[index] {
        if location.x >= frame.minX && location.x <= frame.maxX,
           location.y >= frame.minY && location.y <= frame.maxY {
          return index
        }
      }
    }
    return -1
  }

}

