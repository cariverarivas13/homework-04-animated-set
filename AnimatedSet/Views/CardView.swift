//
//  CardView.swift
//  AnimatedSet
//
//  Created by ApplaudoTest on 28/4/21.
//

import UIKit

class CardView: UIView {

  private var cardToDraw = Cards()
  var isFaceUp = false { didSet  { setNeedsDisplay(); setNeedsLayout() } }
  
  //init where we can set a specified frame for our card and also the card object with all its info
  init(frame: CGRect, card: Cards) {
    self.cardToDraw = card
    super.init(frame: frame)
    self.backgroundColor = UIColor.clear
  }
  
  //required init
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  //function that we use to draw our cards and figures, depending of the info of the card it will draw a certain amounf of either ovals, diamonds and
  //squiggles of three diferent colors and three diferent shades
  override func draw(_ rect: CGRect) {
    super.draw(rect)
    let roundedRect = UIBezierPath(roundedRect: bounds , cornerRadius: bounds.width * 0.1)
    if isFaceUp {
      if cardToDraw.isPicked {
        #colorLiteral(red: 0.6168057467, green: 0.1196166597, blue: 0.1542949035, alpha: 1).setStroke()
        #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).setFill()
        roundedRect.fill()
        roundedRect.lineWidth = bounds.width * 0.08
        roundedRect.stroke()
      } else {
        #colorLiteral(red: 0.5897196531, green: 0.7979089618, blue: 0.7327213883, alpha: 1).setFill()
        roundedRect.fill()
      }
      UIGraphicsGetCurrentContext()?.saveGState()
      roundedRect.addClip()
      switch cardToDraw.shapeType {
      case EnumProperties.TypeOfShape.typeDiamond.rawValue: drawDiamond()
      case EnumProperties.TypeOfShape.typeOval.rawValue: drawOval()
      case EnumProperties.TypeOfShape.typeSquiggle.rawValue: drawSquiggle()
      default:
        break
      }
      UIGraphicsGetCurrentContext()?.restoreGState()
    } else {
      #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).setStroke()
      roundedRect.fill()
    }
  
  }
  
  //function that draws a certain amount of ovals
  func drawOval() {
    var gridCard = Grid(layout: .dimensions(rowCount: cardToDraw.amountOfSymbols, columnCount: 1), frame: bounds)
    gridCard.cellCount = cardToDraw.amountOfSymbols
    for index in (0..<3) {
      if let frame = gridCard[index] {
        let path = UIBezierPath(roundedRect: CGRect(x: frame.origin.x + frame.size.width * 0.1 ,
                                                    y: frame.origin.y + frame.size.height * 0.1,
                                                    width: frame.size.width - frame.size.width * 0.20,
                                                    height: frame.size.height - frame.size.height * 0.20),
                                cornerRadius: 40)
        drawShading(forPath: path, withFrame: frame)
      }
    }
  }
  
  //function that draws a certain number of diamonds
  func drawDiamond() {
    var gridCard = Grid(layout: .dimensions(rowCount: cardToDraw.amountOfSymbols, columnCount: 1), frame: bounds)
    gridCard.cellCount = cardToDraw.amountOfSymbols
    for index in (0..<3) {
      if let frame = gridCard[index] {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: frame.midX, y: frame.minY + frame.size.height * 0.1))
        path.addLine(to: CGPoint(x: frame.maxX - frame.size.width * 0.1, y: frame.midY))
        path.addLine(to: CGPoint(x: frame.midX, y: frame.maxY - frame.size.height * 0.1))
        path.addLine(to: CGPoint(x: frame.minX + frame.size.width * 0.1, y: frame.midY))
        path.close()
        drawShading(forPath: path, withFrame: frame)
      }
    }
  }
  
  //function that draws a certain number of squiggles
  func drawSquiggle() {
    var gridCard = Grid(layout: .aspectRatio(1.5), frame: bounds)
    gridCard.cellCount = cardToDraw.amountOfSymbols
    for index in (0..<3) {
      if let frame = gridCard[index] {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: frame.origin.x + frame.size.width * 0.04, y: frame.origin.y + frame.size.height * 0.38))
        path.addCurve(to: CGPoint(x: frame.origin.x + frame.size.width * 0.32, y: frame.origin.y + frame.size.height * 0.22),
                      controlPoint1: CGPoint(x: frame.origin.x + frame.size.width * 0.06, y: frame.origin.y + frame.size.height * 0.12),
                      controlPoint2: CGPoint(x: frame.origin.x + frame.size.width * 0.16, y: frame.origin.y + frame.size.height * 0.08))
        path.addCurve(to: CGPoint(x: frame.origin.x + frame.size.width * 0.72, y: frame.origin.y + frame.size.height * 0.28),
                      controlPoint1: CGPoint(x: frame.origin.x + frame.size.width * 0.38, y: frame.origin.y + frame.size.height * 0.28),
                      controlPoint2: CGPoint(x: frame.origin.x + frame.size.width * 0.58, y: frame.origin.y + frame.size.height * 0.42))
        path.addCurve(to: CGPoint(x: frame.origin.x + frame.size.width * 0.94, y: frame.origin.y + frame.size.height * 0.32),
                      controlPoint1: CGPoint(x: frame.origin.x + frame.size.width * 0.84, y: frame.origin.y + frame.size.height * 0.12),
                      controlPoint2: CGPoint(x: frame.origin.x + frame.size.width * 0.96, y: frame.origin.y))
        path.addCurve(to: CGPoint(x: frame.origin.x + frame.size.width * 0.42, y: frame.origin.y + frame.size.height * 0.82),
                      controlPoint1: CGPoint(x: frame.origin.x + frame.size.width * 0.92, y: frame.origin.y + frame.size.height * 1.08),
                      controlPoint2: CGPoint(x: frame.origin.x + frame.size.width * 0.50, y: frame.origin.y + frame.size.height * 0.92))
        path.addCurve(to: CGPoint(x: frame.origin.x + frame.size.width * 0.22, y: frame.origin.y + frame.size.height * 0.82),
                      controlPoint1: CGPoint(x: frame.origin.x + frame.size.width * 0.38, y: frame.origin.y + frame.size.height * 0.78),
                      controlPoint2: CGPoint(x: frame.origin.x + frame.size.width * 0.32, y: frame.origin.y + frame.size.height * 0.72))
        path.addCurve(to: CGPoint(x: frame.origin.x + frame.size.width * 0.04, y: frame.origin.y + frame.size.height * 0.38),
                      controlPoint1: CGPoint(x: frame.origin.x, y: frame.origin.y + frame.size.height * 1.08),
                      controlPoint2: CGPoint(x: frame.origin.x, y: frame.origin.y + frame.size.height * 0.58))
        path.close()
        drawShading(forPath: path, withFrame: frame)
      }
    }
  }

  //function that draws a shading to the other figures, this can be either filled, border or stripes
  func drawShading(forPath path: UIBezierPath, withFrame frame: CGRect){
    switch cardToDraw.shadingType {
    case EnumProperties.TypeOfShading.typeFill.rawValue:
      cardToDraw.color.getColorFromString().setFill()
      path.fill()
    case EnumProperties.TypeOfShading.typeBorder.rawValue:
      cardToDraw.color.getColorFromString().setStroke()
      path.lineWidth = frame.width * 0.05
      path.stroke()
    case EnumProperties.TypeOfShading.typeStripes.rawValue:
      addStripes(to: path, to: frame)
      cardToDraw.color.getColorFromString().setStroke()
      path.lineWidth = frame.width * 0.05
      path.stroke()
    default:
      break
    }
  }
  
  //function to add stripes to the other figures
  func addStripes(to path: UIBezierPath, to frame: CGRect) {
    UIGraphicsGetCurrentContext()?.saveGState()
    path.addClip()
    let stripes = UIBezierPath()
    var posX = frame.minX
    while posX < frame.maxX {
      stripes.move(to: CGPoint(x: posX, y: frame.minY))
      stripes.addLine(to: CGPoint(x: posX, y: frame.maxY))
      posX += frame.size.width * 0.15
    }
    stripes.lineWidth = 2
    cardToDraw.color.getColorFromString().setStroke()
    stripes.stroke()
    UIGraphicsGetCurrentContext()?.restoreGState()
  }

}
